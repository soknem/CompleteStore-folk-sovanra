/** @type {import('next').NextConfig} */
const nextConfig = {
    images: {
        domains: ['lh3.googleusercontent.com',"store.istad.co","img.etimg.com","via.placeholder.com","hips.hearstapps.com"],
    },
    output: "standalone",
};

export default nextConfig;
